<?php


namespace Huang\PhpPersonalTools;


class Http
{
    /**
     * @description
     * @param string $url 请求url
     * @param string|array $data 请求数据
     * @param string $authorization header授权
     * @param string $code 请求方式，默认post
     * @return mixed
     */
    public static function rawSend($url, $data, $authorization = '', $code = 'post',$bearer='Bearer')
    {

        $ch = curl_init();   //    初始化一个cURL会话
        curl_setopt($ch, CURLOPT_URL, $url);    // 设置url地址
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  // 禁止curl验证对等证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);   // cURL将终止从服务端进行验证

        switch ($code) {
            case "post":
                $data = is_string($data) ? json_decode($data, true) : $data;
                $data = json_encode($data);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                break;
        }
        $CURLOPT_HTTPHEADER = array('Content-Type: application/json');

        if($authorization){
            $CURLOPT_HTTPHEADER  =array_merge($CURLOPT_HTTPHEADER,array("Authorization: $bearer $authorization"));
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 设置返回格式
        curl_setopt($ch, CURLOPT_HTTPHEADER, $CURLOPT_HTTPHEADER);
        $output = curl_exec($ch);   // 执行一个curl会话
        if (FALSE === $output) {
            echo 'Curl error: ' . curl_error($ch);
        }
        curl_close($ch);            // 关闭一个curl会话
        return json_decode($output, JSON_UNESCAPED_UNICODE);
    }


}
