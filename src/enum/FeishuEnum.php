<?php


namespace Huang\PhpPersonalTools\enum;


class FeishuEnum
{

    const BASE_URL = 'https://open.feishu.cn/';                 // baseUrl

    const ACESSTOKEN_URL = 'open-apis/auth/v3/app_access_token/internal'; //获取accesstoken
    const TENANT_ACESSTOKEN_URL = 'open-apis/auth/v3/tenant_access_token/internal'; //tenant_access_token

    const DEPARTMENTS_URL = 'open-apis/contact/v3/departments'; //获取部门信息列表
    const PARENT_DEPARTMENT_URL = 'open-apis/contact/v3/departments/parent'; //获取父部门信息
    const DEPARTMENT_URL = 'open-apis/contact/v3/departments'; //获取父部门信息

    const EMPLOYEES_URL = 'open-apis/ehr/v1/employees'; //批量获取员工花名册信息
    const EMPLOYEES_USER = 'open-apis/contact/v3/users'; //批量获取员工花
    const USERS = 'open-apis/contact/v3/users'; //获取用户列表
    const SINGLE_USER = 'open-apis/contact/v3/users'; //获取单个用户信息
    const BATCH_GET_URL = 'open-apis/user/v1/batch_get_id' ;// 根据手机号获取 open_id

    const USER_CREATE = 'contact.user.created_v3';  //员工入职
    const USER_DELETE = 'contact.user.deleted_v3';  //员工离职
    const USER_UPDATE = 'contact.user.updated_v3';  //员工信息变更

    const DEPART_CREATE = 'contact.department.created_v3'; // 部门创建
    const DEPART_UPDATE = 'contact.department.updated_v3'; // 部门修改
    const DEPART_DELETE = 'contact.department.deleted_v3'; // 部门删除

    //  员工系统自定义事件
    const STAFF_CRON = 'staff_cron';  // 定时任务事件
    const STAFF_PULL = 'staff_pull';  // 单条拉取事件

    //定义事件id
    const UPDATE_EVENT_ID = 'a00a3adef7eaa5be62ec472cdda82093';
    const PULL_EVENT_ID = 'be34pullf7eaa5be62ec472cdda82093';

    /**
     * 机器人
     */
    const RBOOT_SEND_URL = "https://open.feishu.cn/open-apis/im/v1/messages?receive_id_type=open_id";

    //飞书事件密钥
    const FEISHU_SECRET ='2EeFLh76UZgjy6MLh76Oa0JJD84EdAPL';
}
